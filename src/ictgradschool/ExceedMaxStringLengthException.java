package ictgradschool;

public class ExceedMaxStringLengthException extends Exception {
    public ExceedMaxStringLengthException(String s) {
        super(s);
    }
}
