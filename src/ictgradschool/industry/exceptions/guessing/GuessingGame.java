package ictgradschool.industry.exceptions.guessing;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    /**
     * Plays the actual guessing game.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */
    public void start(String[] args) {

        int number = getRandomValue();
        int guess = 0;

        while (guess != number) {

            guess = getUserGuess(args);

            if (guess > number) {
                System.out.println("Too high!");
            } else if (guess < number) {
                System.out.println("Too low!");
            } else {
                System.out.println("Perfect!");
            }

        }

    }

    /**
     * Gets a random integer between 1 and 100.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */
    private int getRandomValue() {
        return (int) (Math.random() * 100) + 1;
    }

    /**
     * Gets the user's guess from the keyboard. Currently assumes that the user will always enter a valid guess.
     * <p>
     * TODO Implement some error handling, for the cases where the user enters a value that's too big, too small, or
     * TODO not an integer. Change this method so it's guaranteed to return an integer between 1 & 100, inclusive.
     *
     * @param args
     */
    private int getUserGuess(String[] args) {
        int number = getRandomValue();
        int guess = 0;
        while (true) {
            try {
                System.out.print("Enter your guess: ");
                guess = Integer.parseInt(Keyboard.readInput());
                if (guess < 1 || guess > 100) {
                    System.out.println("You typed a number out of range. Please re-enter a valid guess.");
                    //continue; //user entered value from 1-100 inclusive
                }else{
                    break;// 1<= guess<= 100, which is valid
                }
            } catch (NumberFormatException c) {
                System.out.println("You did not type an integer. Please re-enter a valid guess.");

            }
        }
        return guess;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        GuessingGame ex = new GuessingGame();
        ex.start(args);

    }
}

