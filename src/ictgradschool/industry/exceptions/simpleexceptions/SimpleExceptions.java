package ictgradschool.industry.exceptions.simpleexceptions;

import java.util.Scanner;


public class SimpleExceptions {
    public static void main(String[] args) {
        SimpleExceptions exceptions = new SimpleExceptions();
//        String getTitle=new String(title);

        exceptions.handlingException();
//        while (true) {
//            //Question 1 & 2
//            try {
//            } catch (ArithmeticException e) {
//                System.out.println("Number not valid. Please try again.");
//            } catch (NumberFormatException f) {
//                System.out.println("You did not type an integer. Please try again.");
//            }
//            public String getTitle(String title) {
//                if (title==null) {
//                    throw StringIndexOutOfBoundsException("Title cannot be null");
//                }
//                return title.substring(0, getTitle.indexOf(''));
//            }
        //Question3
        try {
            exceptions.Question3();
        } catch (StringIndexOutOfBoundsException soo) {
            System.out.println("This is an invalid String index. Please try again.");
        }

        //Question4
       try{
           exceptions.Question4();
       } catch(ArrayIndexOutOfBoundsException goo) {
           System.out.println("This is an invalid Array index. Please try again.");
       }
    }


    /**
     * The following tries to divide using two user input numbers, but is
     * prone to error.
     */
    public void handlingException() {
        Scanner sc = new Scanner(System.in);


        System.out.print("Enter the first number: ");
        String str1 = sc.next();
        int num1 = Integer.parseInt(str1);
        System.out.print("Enter the second number: ");
        String str2 = sc.next();
        int num2 = Integer.parseInt(str2);

        // Output the result
        try {
            System.out.println("The division of " + num1 + " over " + num2 + " is " + (num1 / num2) + "\n");
        } catch (ArithmeticException ae) {
            System.out.println("Number not valid. Please try again.");
        }
    }

    public void Question3() {
        //Write some Java code which throws a StringIndexOutOfBoundsException
        "Hello".charAt(50);
    }

    public void Question4() {
        //Write some Java code which throws a ArrayIndexOutOfBoundsException
        int[] array=new int[6];
        array[20]=5;
    }
}