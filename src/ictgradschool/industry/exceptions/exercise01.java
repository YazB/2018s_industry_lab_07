package ictgradschool.industry.exceptions;

import ictgradschool.Keyboard;

public class exercise01 {
    private int tryCatch05() {
        int result = 0;
        int[] nums = { 2, 3, 4, -1, 4 };
        try {
            result = nums[nums[3]];
            System.out.println("See you");
        } catch(Exception e) {
            System.out.println("Number error");
            result = -1;
        }
        return result;
    }

    public static void main(String[] args) {
        exercise01 e = new exercise01();
        e.tryCatch05();
    }
}
