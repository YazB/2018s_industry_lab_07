package ictgradschool;

import ictgradschool.Keyboard;


public class ExerciseSix {
    public static void main(String[] args) throws ExceedMaxStringLengthException, InvalidWordException {

        System.out.println("Enter a string of at most 100 characters:");
        String character;


        character = Keyboard.readInput();

        try {
            String[] array = character.split(" +");
            for (String s : array) {
                //System.out.println(s);
                char first = s.charAt(0);
                int x = Character.getNumericValue(first);
                // Check if 'first' is a number. If so, then we throuw out InvalidWordException
                if (x < 10) {
                    throw new InvalidWordException("String contains invalid words");
                }
                try{
                    if (s.length() > 100) {
                        throw new ExceedMaxStringLengthException("String cannot contain this many characters");
                    }
                }catch (ExceedMaxStringLengthException zxce) {
                    System.out.println(zxce.getMessage());
                }
                // We have valid string with <100 in length
                System.out.println("You have entered:"+first);

            }

        } catch (InvalidWordException iwe) {
            System.out.println(iwe.getMessage());
        }


    }
}